## Bobosaur документация модуля

#### Установить в .env файле данные для авторизации с вашей postgres бд
```
EXTERNAL_POSTGRES_DB=
EXTERNAL_POSTGRES_USER=
EXTERNAL_POSTGRES_PWD=
EXTERNAL_POSTGRES_HOST=
```
#### Или используйте наш .env.example
```
cp .env.example .env
```
обращаем внимаение, что в таблице должны быть такие таблицы c соответсвующими данными из датасетов, 
данных вначале хакатона:
bsm_data_train
flight_rasp_data

#### Запустите docker
```
docker compose up -d
```
#### Войдите в графану
http://<your-ip>:3111 (при авторизации ввeдите login: admin, pwd: admin. Установите свои параметры для входа)
#### Откройте dashboard с графиками



