from threading import Timer
from dotenv import load_dotenv
from model import load_company_graphic, load_whole_graphic
load_dotenv()


def main():
    load_company_graphic('b1c49f839832f51f6d687a04db101d4c')
    load_company_graphic('f7adf0ba18d705092fa0a1e2f2c0bafb')
    load_company_graphic('e2fca8135c2fadca093abd79a6b1c0d2')
    load_whole_graphic()
    Timer(60.0, main).start()


main()
