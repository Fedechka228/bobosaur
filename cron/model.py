import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from tqdm import tqdm
from catboost import CatBoostRegressor
from dotenv import load_dotenv

load_dotenv()
import os

tqdm.pandas()

external_database_host = os.getenv("EXTERNAL_POSTGRES_HOST").split(':')[0]
external_database_port = os.getenv("EXTERNAL_POSTGRES_HOST").split(':')[1]
external_database_login = os.getenv("EXTERNAL_POSTGRES_USER")
external_database_database = os.getenv("EXTERNAL_POSTGRES_DB")
external_database_password = os.getenv("EXTERNAL_POSTGRES_PWD")


def load_company_graphic(company_id):
    external_engine = create_engine(
        f"postgresql+psycopg2://{external_database_login}:{external_database_password}@{external_database_host}:{external_database_port}/{external_database_database}"
    )
    connection = external_engine.connect()

    merged_filtred_df = pd.read_sql(f"""
        SELECT
            *
        FROM bsm_data_train
        JOIN
            flight_rasp_data ON bsm_data_train.i_id = flight_rasp_data.i_id
        WHERE 
            flight_rasp_data.cco_hash='{company_id}'
        ORDER BY bsm_data_train.messageprocesseddate DESC LIMIT 20000;
    """, con=connection)

    merged_filtred_df["messageprocesseddate"] = \
        merged_filtred_df["messageprocesseddate"].progress_apply(lambda x: x.floor(freq='T'))

    flights_filtred_df = pd.read_sql(f"""
        SELECT
            *
        FROM flight_rasp_data
        WHERE 
            flight_rasp_data.cco_hash='{company_id}'
        ORDER BY flight_rasp_data.t_st DESC LIMIT 10000;
    """, con=connection)

    # bsm_flights_df.to_csv("test.csv")
    print("DATASETS LOADED")

    merged_filtred_df["counter"] = 1
    test_df = merged_filtred_df.groupby("messageprocesseddate", as_index=False).agg(total_count=("counter", "count"))
    test_df["future_flights"] = 0
    test_df["total_capacity"] = 0

    DAY_PREDICTIONS_START = pd.Timestamp('2023-08-30 00:00:00')
    HOW_MANY = 60 * 24 * 3
    timestamps = [DAY_PREDICTIONS_START]
    for _ in range(HOW_MANY):
        timestamps.append(timestamps[-1] + pd.Timedelta(minutes=1))

    extra_test_df = pd.DataFrame()
    extra_test_df["messageprocesseddate"] = timestamps[1:]
    extra_test_df["total_count"] = [0] * HOW_MANY
    extra_test_df["future_flights"] = 0
    extra_test_df["total_capacity"] = 0

    test_df = pd.concat([test_df, extra_test_df], ignore_index=False)

    flights_filtred_df["t_st"] = pd.to_datetime(flights_filtred_df["t_st"])

    for i, row in tqdm(flights_filtred_df.iterrows(), total=flights_filtred_df.shape[0]):
        test_df.loc[(test_df["messageprocesseddate"] > (row["t_st"] - pd.Timedelta(hours=3))) &
                    (test_df["messageprocesseddate"] < row["t_st"]), "future_flights"] += 1
        test_df.loc[(test_df["messageprocesseddate"] > (row["t_st"] - pd.Timedelta(hours=3))) &
                    (test_df["messageprocesseddate"] < row["t_st"]), "total_capacity"] += row["config"]

    test_df["day_of_week"] = test_df["messageprocesseddate"].map(lambda x: x.day_of_week)
    test_df["is_weekday"] = test_df["day_of_week"].isin([6, 7])
    test_df["hour"] = test_df["messageprocesseddate"].map(lambda x: x.hour)

    df = test_df.copy()
    for column in ["total_count", "future_flights", "total_capacity"]:
        for i in range(10, 60):
            df[f"{column}_lag{i}"] = test_df[column].shift(i)

    print("TEST DATA CREATED")

    df = df.dropna()

    regs = [CatBoostRegressor().load_model(f"reg{i}.cbm", format="cbm") for i in range(5)]

    # for_preds_df = df[df["messageprocesseddate"] >= DAY_PREDICTIONS_START - pd.Timedelta(minutes=100)
    #                   ].drop(columns=["messageprocesseddate", "total_count"]).copy()
    column_names = [f"total_count_lag{i}" for i in range(10, 60)]
    cur_count_lags = list(df[column_names].iloc[0])
    for i in range(10):
        cur_count_lags = [df[column_names[0]].iloc[i + 1]] + cur_count_lags
    print(cur_count_lags)

    iteration = 0
    total_preds = []
    timestamps = []
    for i, row in tqdm(df.iterrows(), total=df.shape[0]):
        iteration += 1
        if iteration <= 10:
            continue
        if iteration == HOW_MANY:
            break
        for column, value in zip(column_names, cur_count_lags[10:10 + 60 + 1]):
            row[column] = value
        pred = np.mean([reg.predict(row.drop(labels=["messageprocesseddate", "total_count"])) for reg in regs])
        total_preds.append(pred)
        cur_count_lags = [pred] + cur_count_lags
        timestamps.append(row["messageprocesseddate"])

    show_df = pd.DataFrame()
    show_df["forecast"] = total_preds
    show_df["upper_bound"] = [x + 5 for x in total_preds]
    show_df["lower_bound"] = [x - 5 for x in total_preds]
    show_df["timestamp"] = timestamps

    show_df.to_sql(name=f'prediction_{company_id}', con=external_engine, if_exists='replace')

    connection.close()
    external_engine.dispose()


def load_whole_graphic():
    external_engine = create_engine(
        f"postgresql+psycopg2://{external_database_login}:{external_database_password}@{external_database_host}:{external_database_port}/{external_database_database}"
    )
    connection = external_engine.connect()

    merged_filtred_df = pd.read_sql(f"""
        SELECT
            *
        FROM bsm_data_train
        JOIN
            flight_rasp_data ON bsm_data_train.i_id = flight_rasp_data.i_id
        ORDER BY bsm_data_train.messageprocesseddate DESC LIMIT 3000;
    """, con=connection)

    merged_filtred_df["messageprocesseddate"] = \
        merged_filtred_df["messageprocesseddate"].progress_apply(lambda x: x.floor(freq='T'))

    flights_filtred_df = pd.read_sql(f"""
        SELECT
            *
        FROM flight_rasp_data
        ORDER BY flight_rasp_data.t_st DESC LIMIT 24000;
    """, con=connection)

    # bsm_flights_df.to_csv("test.csv")
    print("DATASETS LOADED")

    merged_filtred_df["counter"] = 1
    test_df = merged_filtred_df.groupby("messageprocesseddate", as_index=False).agg(total_count=("counter", "count"))
    test_df["future_flights"] = 0
    test_df["total_capacity"] = 0

    DAY_PREDICTIONS_START = pd.Timestamp('2023-08-30 00:00:00')
    HOW_MANY = 60 * 24 * 3
    timestamps = [DAY_PREDICTIONS_START]
    for _ in range(HOW_MANY):
        timestamps.append(timestamps[-1] + pd.Timedelta(minutes=1))

    extra_test_df = pd.DataFrame()
    extra_test_df["messageprocesseddate"] = timestamps[1:]
    extra_test_df["total_count"] = [0] * HOW_MANY
    extra_test_df["future_flights"] = 0
    extra_test_df["total_capacity"] = 0

    test_df = pd.concat([test_df, extra_test_df], ignore_index=False)

    flights_filtred_df["t_st"] = pd.to_datetime(flights_filtred_df["t_st"])

    for i, row in tqdm(flights_filtred_df.iterrows(), total=flights_filtred_df.shape[0]):
        test_df.loc[(test_df["messageprocesseddate"] > (row["t_st"] - pd.Timedelta(hours=3))) &
                    (test_df["messageprocesseddate"] < row["t_st"]), "future_flights"] += 1
        test_df.loc[(test_df["messageprocesseddate"] > (row["t_st"] - pd.Timedelta(hours=3))) &
                    (test_df["messageprocesseddate"] < row["t_st"]), "total_capacity"] += row["config"]

    test_df["day_of_week"] = test_df["messageprocesseddate"].map(lambda x: x.day_of_week)
    test_df["is_weekday"] = test_df["day_of_week"].isin([6, 7])
    test_df["hour"] = test_df["messageprocesseddate"].map(lambda x: x.hour)

    df = test_df.copy()
    for column in ["total_count", "future_flights", "total_capacity"]:
        for i in range(10, 60):
            df[f"{column}_lag{i}"] = test_df[column].shift(i)

    print("TEST DATA CREATED")

    df = df.dropna()

    regs = [CatBoostRegressor().load_model(f"reg{i}.cbm", format="cbm") for i in range(5)]

    # for_preds_df = df[df["messageprocesseddate"] >= DAY_PREDICTIONS_START - pd.Timedelta(minutes=100)
    #                   ].drop(columns=["messageprocesseddate", "total_count"]).copy()
    column_names = [f"total_count_lag{i}" for i in range(10, 60)]
    cur_count_lags = list(df[column_names].iloc[0])
    for i in range(10):
        cur_count_lags = [df[column_names[0]].iloc[i + 1]] + cur_count_lags
    print(cur_count_lags)

    iteration = 0
    total_preds = []
    timestamps = []
    for i, row in tqdm(df.iterrows(), total=df.shape[0]):
        iteration += 1
        if iteration <= 10:
            continue
        if iteration == HOW_MANY:
            break
        for column, value in zip(column_names, cur_count_lags[10:10 + 60 + 1]):
            row[column] = value
        pred = np.mean([reg.predict(row.drop(labels=["messageprocesseddate", "total_count"])) for reg in regs])
        total_preds.append(pred)
        cur_count_lags = [pred] + cur_count_lags
        timestamps.append(row["messageprocesseddate"])

    show_df = pd.DataFrame()
    show_df["forecast"] = total_preds
    show_df["upper_bound"] = [x + 5 for x in total_preds]
    show_df["lower_bound"] = [x - 5 for x in total_preds]
    show_df["timestamp"] = timestamps

    try:
        show_df.to_sql(name='main_prediction', con=external_engine, if_exists='replace')
    except NameError:
        print(NameError)

    connection.close()
    external_engine.dispose()
